<?php

use Illuminate\Support\Facades\Route;
use Workerman\Worker;
use Illuminate\Support\Facades\App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/socket', function () {


    $direbase = app('firebase.firestore')
    ->database()
    ->collection('agroconsultant')
    ->document('9KBD0IUqITsm4iTyvx6E');

// Data to be sent to Firestore
$data = [
    'name' => 'Farrukh Choriyev',
    'email' => 'john.doe@example.com',
    // Add other fields as needed
];

// Add the data to Firestore
$direbase->set($data);
});

Route::get('/bla', [\App\Http\Controllers\WebsocketController::class, 'index']);


