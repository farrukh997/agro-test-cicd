<?php

namespace App\Singleton;

class Connections
{
    public array $connections;

    public function store($user_id, $connection)
    {
        $this->connections[$user_id] = $connection;
        info('conn',$this->connections);
        return $this->connections;
    }

    public function send($user_id)
    {
        return $this->connections[$user_id];
    }

    public static function socket($user_id)
    {
        return app(Connections::class)->send($user_id);
    }

    public static function storeConn($user_id, $connection)
    {
        return app(Connections::class)->store($user_id, $connection);
    }

}
