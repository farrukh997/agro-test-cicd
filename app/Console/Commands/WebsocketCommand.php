<?php

namespace App\Console\Commands;

use App\Singleton\Connections;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Workerman\Connection\TcpConnection;
use Workerman\Worker;
class WebsocketCommand extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'socket:web';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Websocket start';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $ws_worker = new Worker('websocket://127.0.0.1:8011');
        // dd($ws_worker);
        $ws_worker->count = 4;
        $ws_worker->onConnect = function ($connection) {
            $conn = Connections::storeConn($connection->id, $connection);
            echo "New connection:  " . $connection->id ."\n";
            // broadcast(new \App\Events\WebSocketEvent('laravelchannel', ['message' => 'websocket']));
        };

        // Emitted when data received
        $ws_worker->onMessage = function (TcpConnection $connection, $data) {
            // Send hello $data
            echo "Message " . $data;
            $conn = Connections::socket(1);
            $conn->send('data singleton' . ' hello man');
            info($data);
            $connection->send('data ' . $data);
        };

        // Emitted when connection closed
        $ws_worker->onClose = function ($connection) {
            echo "Connection closed\n";
        };
        Worker::runAll();
    }
}
